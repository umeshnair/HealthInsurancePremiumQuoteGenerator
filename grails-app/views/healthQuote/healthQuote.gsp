<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Health Insurance Premium Quote Generator</title>
		
		<script type="text/javascript">
			function checkValues()
			{
				var chealth = [0,0,0,0];
				var habits = [0,0,0,0];

				if($("#hyper").prop('checked') == true){
					chealth[0] = 1;
				} else {
					chealth[0] = 0;
				}

				if($("#bloodp").prop('checked') == true){
					chealth[1] = 1;
				} else {
					chealth[1] = 0;
				}

				if($("#bloods").prop('checked') == true){
					chealth[2] = 1;
				} else {
					chealth[2] = 0;
				}

				if($("#overw").prop('checked') == true){
					chealth[3] = 1;
				} else {
					chealth[3] = 0;
				}	

				if($("#smoke").prop('checked') == true){
					habits[0] = 1;
				} else {
					habits[0] = 0;
				}

				if($("#alcohol").prop('checked') == true){
					habits[1] = 1;
				} else {
					habits[1] = 0;
				}

				if($("#dailye").prop('checked') == true){
					habits[2] = 1;
				} else {
					habits[2] = 0;
				}

				if($("#drugs").prop('checked') == true){
					habits[3] = 1;
				} else {
					habits[3] = 0;
				}	

				$("#hidCHealth").val(chealth);
				$("#hidHabits").val(habits);
			}
		</script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">&nbsp;</div> 
		  	<div class="panel panel-default">
		  		<div class="panel-heading"><h1 class="text-center">Health Insurance Premium Quote</h1></div>	
		  	  
			
			 <div class="panel-body"">
				<form class="form-horizontal" action="generateQuote">
					  <div class="form-group row" style="margin:10px;">		
					  	<div class="input-group col-sm-4">			  					  	 
					  	 <span class="input-group-addon"><strong>Name</strong></span>
						 <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name here..." required>		
						 </div>				 
					  </div>			
					  <div class="form-group row" style="margin:10px;">
					  	<div class="input-group col-sm-4">		
					  		<span class="input-group-addon"><strong>Gender</strong></span>			  	 
					    	<select class="form-control" id="gender" name="gender">
							    <option>Male</option>
							    <option>Female</option>
							    <option>Transgender</option>
							  </select>
						</div>
					  </div>
					  <div class="form-group row" style="margin:10px;">					  	
					  	 <div class="input-group col-sm-4"> 
					  	 	<span class="input-group-addon"><strong>Age</strong></span>	
					    	<input type="number" class="form-control" id="age" name="age" min="1" placeholder="Enter your age..." required>
						 </div>   
					  </div>	
					  <div class="form-group row">	
						  	<div class="col-sm-4"> 	 
						  		<fieldset style="margin-left:10px; border: solid 1px #DDD !important; padding: 0 10px 10px 10px; border-bottom: none;">
	    							<legend style=" width: auto !important; border: none; font-size: 14px;">Current Health</legend> 
								  <div class="checkbox">
								    <label><input type="checkbox" id="hyper" name="hyper">Hypertension</label>
								  </div>
								  <div class="checkbox">
									  <label><input type="checkbox" id="bloodp" name="bloodp">Blood pressure</label>
									</div>
									<div class="checkbox">
									  <label><input type="checkbox" id="bloods" name="bloods">Blood sugar</label>
									</div>
									<div class="checkbox">
									  <label><input type="checkbox" id="overw" name="overw">Overweight</label>
									</div>
									<g:hiddenField name="hidCHealth" id="hidCHealth" value="" />
								</fieldset>
							</div>
						</div>
						<div class="form-group row">	
						  	<div class="col-sm-4"> 	 
						  		<fieldset style="margin-left:10px; border: solid 1px #DDD !important; padding: 0 10px 10px 10px; border-bottom: none;">
	    							<legend style=" width: auto !important; border: none; font-size: 14px;">Habits</legend> 
								  <div class="checkbox">
								    <label><input type="checkbox" id="smoke" name="smoke">Smoking</label>
								  </div>
								  <div class="checkbox">
									  <label><input type="checkbox" id="alcohol" name="alcohol">Alcohol</label>
									</div>
									<div class="checkbox">
									  <label><input type="checkbox" id="dailye" name="dailye">Daily Exercise</label>
									</div>
									<div class="checkbox">
									  <label><input type="checkbox" id="drugs" name="drugs">Drugs</label>
									</div>
									<g:hiddenField name="hidHabits" id="hidHabits" value="" />
								</fieldset>
							</div>
						</div>
						<div class="form-group row" style="margin:10px;">	
					  		<button type="submit" class="btn btn-primary" onclick="return checkValues();">Get Quote</button>
					  		<g:link url="${request.contextPath}">
								<button type="button" class="btn btn-primary" style="margin-left:20px;">Home</button>
							</g:link>
					  	</div>	
				</form>
			</div>			
			<div class="panel-footer"><div class="row text-right">Copyright 2017-18 Emids</div></div> 	
			</div>			
		</div>
	</body>
</html>
