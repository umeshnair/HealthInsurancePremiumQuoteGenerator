<!DOCTYPE html>
<%@page import="java.text.DecimalFormat"%>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Health Insurance Premium Quote for ${name}</title>
		
		<script type="text/javascript">
		$(document).ready(function() {
			if(${currentHealth[0]} == '1')
		    {
			    $("#hyper").prop("checked", true);
			}
			if(${currentHealth[1]} == '1')
		    {
			    $("#bloodp").prop("checked", true);
			}
			if(${currentHealth[2]} == '1')
		    {
			    $("#bloods").prop("checked", true);
			}
			if(${currentHealth[3]} == '1')
		    {
			    $("#overw").prop("checked", true);
			}

			if(${currentHabits[0]} == '1')
		    {
			    $("#smoke").prop("checked", true);
			}
			if(${currentHabits[1]} == '1')
		    {
			    $("#alcohol").prop("checked", true);
			}
			if(${currentHabits[2]} == '1')
		    {
			    $("#dailye").prop("checked", true);
			}
			if(${currentHabits[3]} == '1')
		    {
			    $("#drugs").prop("checked", true);
			}
		});				
		</script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">&nbsp;</div> 
		  	<div class="panel panel-default">
		  		<%
  					DecimalFormat twoDForm = new DecimalFormat("#.00");  
				%>
		  		<div class="panel-heading"><h1 class="text-center">Health Insurance Premium for ${name}: ₹ ${twoDForm.format(finalPremium)}</h1></div>	
		  	  
			
			 <div class="panel-body">	
			 	<div class="col-sm-6">			
				  <div class="form-group row" style="margin:10px;">		
				  	<div class="input-group col-sm-8">			  					  	 
				  	 <span class="input-group-addon"><strong>Name</strong></span>
					 <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name here..." value="${name}" disabled="disabled">		
					 </div>				 
				  </div>			
				  <div class="form-group row" style="margin:10px;">
				  	<div class="input-group col-sm-8">		
				  		<span class="input-group-addon"><strong>Gender</strong></span>			  	 
				    	<select class="form-control" id="gender" name="gender" value="${gender}" disabled="disabled">
						    <option>Male</option>
						    <option>Female</option>
						    <option>Transgender</option>
						  </select>
					</div>
				  </div>
				  <div class="form-group row" style="margin:10px;">					  	
				  	 <div class="input-group col-sm-8"> 
				  	 	<span class="input-group-addon"><strong>Age</strong></span>	
				    	<input type="number" class="form-control" id="age" name="age" min="1" placeholder="Enter your age..." value="${age}" disabled="disabled">
					 </div>   
				  </div>	
				  <div class="form-group row">	
					  	<div class="col-sm-8"> 	 
					  		<fieldset style="margin-left:10px; border: solid 1px #DDD !important; padding: 0 10px 10px 10px; border-bottom: none;">
    							<legend style=" width: auto !important; border: none; font-size: 14px;">Current Health</legend> 
							  <div class="checkbox">
							    <label><input type="checkbox" id="hyper" name="hyper" disabled="disabled">Hypertension</label>
							  </div>
							  <div class="checkbox">
								  <label><input type="checkbox" id="bloodp" name="bloodp" disabled="disabled">Blood pressure</label>
								</div>
								<div class="checkbox">
								  <label><input type="checkbox" id="bloods" name="bloods" disabled="disabled">Blood sugar</label>
								</div>
								<div class="checkbox">
								  <label><input type="checkbox" id="overw" name="overw" disabled="disabled">Overweight</label>
								</div>
								<g:hiddenField name="hidCHealth" id="hidCHealth" value="" />
							</fieldset>
						</div>
					</div>
					<div class="form-group row">	
					  	<div class="col-sm-8"> 	 
					  		<fieldset style="margin-left:10px; border: solid 1px #DDD !important; padding: 0 10px 10px 10px; border-bottom: none;">
    							<legend style=" width: auto !important; border: none; font-size: 14px;">Habits</legend> 
							  <div class="checkbox">
							    <label><input type="checkbox" id="smoke" name="smoke" disabled="disabled">Smoking</label>
							  </div>
							  <div class="checkbox">
								  <label><input type="checkbox" id="alcohol" name="alcohol" disabled="disabled">Alcohol</label>
								</div>
								<div class="checkbox">
								  <label><input type="checkbox" id="dailye" name="dailye" disabled="disabled">Daily Exercise</label>
								</div>
								<div class="checkbox">
								  <label><input type="checkbox" id="drugs" name="drugs" disabled="disabled">Drugs</label>
								</div>
								<g:hiddenField name="hidHabits" id="hidHabits" value="" />
							</fieldset>
						</div>
					</div>
					<div class="form-group row" style="margin:10px;">	
				  		&nbsp;
				  	</div>	
				</div>	
				<div class="col-sm-6">
					<div class="row" style="border-radius: 10px 10px 10px 10px;-moz-border-radius: 10px 10px 10px 10px;
					-webkit-border-radius: 10px 10px 10px 10px; border: 0px solid #000000;
					-webkit-box-shadow: 2px 4px 4px 1px rgba(163,163,163,0.69);
					-moz-box-shadow: 2px 4px 4px 1px rgba(163,163,163,0.69);
					box-shadow: 2px 4px 4px 1px rgba(163,163,163,0.69);">
						<div class="panel panel-default">
							<div class="panel-heading"><h5 class="text-center">Premium Summary</h5></div>
							<div class="panel-body">
								<table class="table table-bordered">
								    <thead>
								      <tr>
								        <th>Item</th>
								        <th>Price (in rupees)</th>
								      </tr>
								    </thead>
								    <tfoot>
									    <tr style="font-weight:bold;">
									      <td>Total</td>
									      <td class="text-right">${twoDForm.format(finalPremium)}</td>
									    </tr>
									  </tfoot>
								    <tbody>
								      <tr>
								        <td>Base Premium</td>
								        <td class="text-right">${twoDForm.format(premium)}</td>
								      </tr>
								      <tr>
								        <td>CGST @ ${cgst+'%'}</td>
								        <td class="text-right">${twoDForm.format(cgstOnP)}</td>
								      </tr>
								      <tr>
								        <td>SGST @ ${sgst+'%'}</td>
								        <td class="text-right">${twoDForm.format(sgstOnP)}</td>
								      </tr>
								      <tr>
								        <td>CESS @ ${cess+'%'}</td>
								        <td class="text-right">${twoDForm.format(cessOnT)}</td>
								      </tr>
								    </tbody>
								  </table>
							</div>
						</div>
						<div class="row text-right">
							<g:link action="healthQuote" controller="healthQuote">
								<button type="button" class="btn btn-primary" style="margin-right:10px;">Start Over</button>								
							</g:link>
							<g:link url="${request.contextPath}">
								<button type="button" class="btn btn-primary" style="margin-right:20px;">Home</button>
							</g:link>
						</div>
						<div class="row">&nbsp;</div>
					</div>
				</div>	  					
			</div>			
			<div class="panel-footer"><div class="row text-right">Copyright 2017-18 Emids</div></div> 	
			</div>			
		</div>
	</body>
</html>
