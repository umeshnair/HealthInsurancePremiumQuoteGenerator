<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Health Insurance Premium Quote Generator</title>
		
	</head>
	<body>
		<div class="container-fluid">
			<div class="panel panel-default">
			<p>
				<h1 class="text-center">Health Insurance Premium Quote Generator</h1>
				<form action="healthQuote/healthQuote" class="text-center">
					<div class="row">
						<img src="images/Health-Insurance-Premium.jpg" alt="Health-Insurance-Premium" class="img-rounded"/>
					</div>
					<div class="row">
						<div class="col-sm-4">
						&nbsp;
						</div>
					</div>
					<div class="row text-center">
						<div class="col-sm-4">
						</div>
						<div class="col-sm-4">
							<button type="submit" class="btn btn-primary btn-block">Start</button>
						</div>
						<div class="col-sm-4">
						</div>
					</div>
				</form>
			</p>			
			</div>		
		</div>
	</body>
</html>
