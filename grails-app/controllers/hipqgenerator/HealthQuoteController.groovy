/**********************************************************************************
**
** HealthInsurancePremiumQuoteGenerator - HealthQuoteController.groovy
**
** Copyright (c) 2017-2018 EMIDS Corporation . All Rights Reserved.
**
** This software is the confidential and proprietary information of EMIDS Corporation 
** ("Confidential Information"). You shall not disclose such Confidential
** Information and shall use it only in accordance with the terms of the
** license agreement you entered into with EMIDS.
**
** EMIDS MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
** THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
** TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
** PARTICULAR PURPOSE, OR NON-INFRINGEMENT. EMIDS SHALL NOT BE LIABLE FOR
** ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
** DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
** 
**
** Version Information
** ------------------|---------------|------------|--------------------------------
** Date              | Author        | Version    | Comments   
** ------------------|---------------|------------|--------------------------------
** 15-Aug-2017       | Umesh V S     | 1.0        | First Creation    
** ------------------|---------------|------------|--------------------------------
** 21-Aug-2017       | Umesh V S     | 1.0        | Added copyright information
**********************************************************************************/

package hipqgenerator

class HealthQuoteController {
	private static basePremium = 5000
	private static cgst = 0;
	private static sgst = 0;
	private static cess = 0;

    def index() { }
	
	def healthQuote() {}
	
	def generateQuote() {
		//println "In generateQuote:" + params;
		
		float premium = 0; 
		
		String name = params.name;
		String gender = params.gender;
		int age = params.age.toInteger();
		def currentHealth = params.hidCHealth.split(",");
		def currentHabits = params.hidHabits.split(",");		
		
		def hypertension = currentHealth[0]
		def bloodPressure = currentHealth[1]
		def bloodSugar = currentHealth[2]
		def overweight = currentHealth[3]
		
		def smoking = currentHabits[0]	
		def alcohol = currentHabits[1]
		def dailyExercise = currentHabits[2]
		def drugs = currentHabits[3]
		
		/////////// premium according to age		
		premium = basePremium;
		
		if(age >= 18) {
			premium += premium * 0.1;
		}
		if(age >= 25 && age < 40) {
			def overAge25 = 25;			
			
			while (overAge25 <= age) {
				premium += (premium * 0.1);
				overAge25 += 5;
			}
		}
		if(age >= 40) {
			/////////Over 40 years premium increases 20% of base every 5 years
			def overAge40 = 40;			
			
			while (overAge40 <= age) {
				premium += (premium * 0.2);
				overAge40 += 5;
			}
		}
		
		//////////// additional premium for male customers
		if(gender.equalsIgnoreCase("male")){
			premium += premium * 0.02;
		}		
		/////////// additional premium for pre-existing medical conditions
		def preexist = 0;
		if(hypertension.toInteger() == 1) {
			preexist++;
		}
		if(bloodPressure.toInteger() == 1) {
			preexist++;
		}
		if(bloodSugar.toInteger() == 1) {
			preexist++;
		}
		if(overweight.toInteger() == 1) {
			preexist++;
		}	
		premium += premium * preexist / 100;
		////////// additional premium for bad habits
		def habit = 0;
		if(smoking.toInteger() == 1) {
			habit += 3;
		}
		if(alcohol.toInteger() == 1) {
			habit += 3;
		}
		if(drugs.toInteger() == 1) {
			habit += 3;
		}		
		////////// less premium for good habits
		if(dailyExercise.toInteger() == 1) {
			habit -= 3;
		}
		premium += premium * habit / 100;
		
		premium = Math.ceil(premium);
		println "premium:"+premium
		
		def cgstOnP = premium * (cgst / 100);
		def sgstOnP = premium * (sgst / 100);
		def cessOnT = (cgstOnP + sgstOnP) * (cess / 100);
		
		def finalPremium = premium + cgstOnP + sgstOnP + cessOnT
		
		//println "premium:"+premium
		[premium:premium, finalPremium:finalPremium, name:name, gender:gender, age:age, currentHealth:currentHealth, currentHabits:currentHabits, 
			cgst:cgst, sgst:sgst, cess:cess, cgstOnP:cgstOnP, sgstOnP:sgstOnP, cessOnT:cessOnT]
	}
}
