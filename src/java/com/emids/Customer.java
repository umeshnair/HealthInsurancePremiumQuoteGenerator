package com.emids;

public class Customer {
	
	private String name;
	private String gender;
	private int age;
	private boolean hyperTension;
	private boolean bloodPressure;
	private boolean bloodSugar;
	private boolean overWeight;
	private boolean smoking;
	private boolean alochol;	
	private boolean exercise;
	private boolean drugs;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isHyperTension() {
		return hyperTension;
	}

	public void setHyperTension(boolean hyperTension) {
		this.hyperTension = hyperTension;
	}	

	public boolean isBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public boolean isBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	public boolean isOverWeight() {
		return overWeight;
	}

	public void setOverWeight(boolean overweight) {
		this.overWeight = overweight;
	}

	public boolean isSmoking() {
		return smoking;
	}

	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}

	public boolean isAlochol() {
		return alochol;
	}

	public void setAlochol(boolean alochol) {
		this.alochol = alochol;
	}

	public boolean isExercise() {
		return exercise;
	}

	public void setExercise(boolean exercise) {
		this.exercise = exercise;
	}

	public boolean isDrugs() {
		return drugs;
	}

	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}

	public Customer(String name, String gender, int age, boolean hyperTension,
			boolean bloodpressure, boolean sugar, boolean overweight,
			boolean smoking, boolean alochol, boolean exercise, boolean drugs) {
		super();
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.hyperTension = hyperTension;
		this.bloodPressure = bloodpressure;
		this.bloodSugar = sugar;
		this.overWeight = overweight;
		this.smoking = smoking;
		this.alochol = alochol;
		this.exercise = exercise;
		this.drugs = drugs;
	}
}
